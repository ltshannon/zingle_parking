//
//  Zingle_ParkingAppDelegate.h
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuVC.h"
#import "DataSupplier.h"

@interface AppDelegate : NSObject <UIApplicationDelegate> {
    IBOutlet MenuVC *menuVC;
    
    id fbController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) MenuVC *menuVC;
@property (nonatomic, retain) id fbController;
-(void)showPinAlert;
-(void)showZingleMenu;
-(void)goBackToHomeScreen;

@end
