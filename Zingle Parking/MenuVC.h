//
//  zingle_restaurantsViewController.h
//  zingle restaurants
//
//  Created by Andrew on 6/8/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC : UIViewController {
    
    IBOutlet UIButton *settingsBtn;
}
- (IBAction)goToMap:(id)sender;
- (IBAction)goToMyLocations:(id)sender;
- (IBAction)goToMyProfile:(id)sender;
- (IBAction)activateFacebook:(id)sender;
- (IBAction)activateTwitter:(id)sender;
- (IBAction)goToSettings:(id)sender;

@end
