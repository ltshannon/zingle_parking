//
//  TwitterVC.m
//  Zingle Parking
//
//  Created by Andrew on 6/24/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "TwitterVC.h"
#import "Constants.h"
#import "SA_OAuthTwitterEngine.h"
#import <QuartzCore/QuartzCore.h>

@implementation TwitterVC

- (void)dealloc
{
    [engine release];
    [tweetTf release];
    [panelVw release];
    [tweetBtn release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [[panelVw layer] setCornerRadius:20];
    [[panelVw layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[panelVw layer] setBorderWidth:1];
}

- (void)viewDidUnload
{
    [tweetTf release];
    tweetTf = nil;
    [panelVw release];
    panelVw = nil;
    [tweetBtn release];
    tweetBtn = nil;
    [super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    if (engine) return;
	engine = [[[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate: self] retain];
	engine.consumerKey = kOAuthConsumerKey;
	engine.consumerSecret = kOAuthConsumerSecret;
	
	SA_OAuthTwitterController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine: engine delegate: self];
	
	if (controller) {
       // controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
		[self presentModalViewController: controller animated:NO];
    }
    
	//else {
        
		//[engine sendUpdate: [NSString stringWithFormat: @"Already Updated. %@", [NSDate date]]];
	//}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)sendTweet:(id)sender {
    [tweetTf resignFirstResponder];
    if (engine) {
        if ([tweetTf.text length] < 1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Blank Tweet!" message:@"Your message is blank.  Please enter a message in the textfield." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            return;
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        tweetBtn.alpha = 0.5;
        tweetBtn.enabled = NO;
        [engine sendUpdate:tweetTf.text];
    }
    
}

#pragma mark -
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//=============================================================================================================================
#pragma mark SA_OAuthTwitterEngineDelegate
- (void) storeCachedTwitterOAuthData: (NSString *) data forUsername: (NSString *) username {
	NSUserDefaults			*defaults = [NSUserDefaults standardUserDefaults];
    
	[defaults setObject: data forKey: @"authData"];
	[defaults synchronize];
}

- (NSString *) cachedTwitterOAuthDataForUsername: (NSString *) username {
	return [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"];
}

//=============================================================================================================================
#pragma mark SA_OAuthTwitterControllerDelegate
- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) username {
	NSLog(@"Authenicated for %@", username);
}

- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller {
	NSLog(@"Authentication Failed!");
    [self dismissModalViewControllerAnimated:NO];
    [self dismissModalViewControllerAnimated:YES];
}

- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller {
	NSLog(@"Authentication Canceled.");
    [self dismissModalViewControllerAnimated:NO];
    [self dismissModalViewControllerAnimated:YES];
}

//=============================================================================================================================
#pragma mark TwitterEngineDelegate
- (void) requestSucceeded: (NSString *) requestIdentifier {
	tweetTf.text = @"";
    tweetBtn.alpha = 1.0;
    tweetBtn.enabled = YES;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tweet Successful:" message:nil delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void) requestFailed: (NSString *) requestIdentifier withError: (NSError *) error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tweet Failed:" message:[error description] delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
	//NSLog(@"Request %@ failed with error: %@", requestIdentifier, error);
    tweetBtn.alpha = 1.0;
    tweetBtn.enabled = YES;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}



//=============================================================================================================================

@end
