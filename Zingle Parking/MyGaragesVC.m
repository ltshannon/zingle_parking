//
//  MyGaragesVC.m
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "MyGaragesVC.h"
#import <QuartzCore/QuartzCore.h>
#import "DetailedGarageVC.h"
#import "ProfileVC.h"
#import "Constants.h"
#import "MyGarageDataEntryVC.h"

@implementation MyGaragesVC
@synthesize profileDelegate;

- (void)dealloc
{
    [garages release];
    [profileDelegate release];
    [myTable release];
    [editBtn release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[myTable layer] setCornerRadius:20];
    [[myTable layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[myTable layer] setBorderWidth:1];
 
    self.title = @"My Garages";
    
    if (profileDelegate) {
        [editBtn removeFromSuperview];
    }
}
- (void)viewDidUnload
{
    [profileDelegate release];
    profileDelegate = nil;
    [garages release];
    garages = nil;
    [myTable release];
    myTable = nil;
    [editBtn release];
    editBtn = nil;
    [super viewDidUnload];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    garages = [[[NSUserDefaults standardUserDefaults] objectForKey:kFavoriteGarages] mutableCopy];
    
    if (!garages) {
        garages = [[NSMutableArray alloc] initWithCapacity:0];
    }
    [garages retain];
    
    [myTable reloadData];
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [garages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    NSDictionary *g = [garages objectAtIndex:indexPath.row];
    cell.textLabel.text = [g objectForKey:kFacName];
    cell.detailTextLabel.text = [[g objectForKey:kFacAddress] objectForKey:kFacAddressStreet];
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (profileDelegate) {
        return NO;
    }
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [garages removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [[NSUserDefaults standardUserDefaults] setObject:garages forKey:kFavoriteGarages];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    if (!garages) {
        return;
    }
    
    NSDictionary *g = [[garages objectAtIndex:fromIndexPath.row] retain];
    [garages removeObject:g];
    [garages insertObject:g atIndex:toIndexPath.row];
    [g release];
    
    [[NSUserDefaults standardUserDefaults] setObject:garages forKey:kFavoriteGarages];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (profileDelegate) {
        return NO;
    }
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (profileDelegate && [profileDelegate respondsToSelector:@selector(setGarageName:)]) {
        [profileDelegate setGarageName:[[garages objectAtIndex:indexPath.row] objectForKey:kFacName]];
        [self dismissModalViewControllerAnimated:YES];
    } else {
        DetailedGarageVC *detailViewController = [[DetailedGarageVC alloc] initWithNibName:@"DetailedGarageVC" bundle:nil];
        detailViewController.garage = [garages objectAtIndex:indexPath.row];
        detailViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentModalViewController:detailViewController animated:YES];
        [detailViewController release];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    // user wants to add more details.
    MyGarageDataEntryVC *viewController = [[MyGarageDataEntryVC alloc] initWithNibName:@"MyGarageDataEntryVC" bundle:nil];
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    viewController.myGarage = [[garages objectAtIndex:indexPath.row] mutableCopy];
    [self presentModalViewController:viewController animated:YES];
    [viewController release];
}

#pragma mark IBActions

- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)editMyGarages:(id)sender {
    [myTable setEditing:!myTable.editing];
}
@end
