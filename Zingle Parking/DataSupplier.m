//
//  DataSupplier.m
//  EasyPark
//
//  Created by Andrew Schenk on 7/24/11.
//  Copyright 2011 Mobile Parking Apps. All rights reserved.
//

#import "DataSupplier.h"
#import <CoreLocation/CoreLocation.h>
#import "Constants.h"
#import "JSONKit.h"

static DataSupplier *sharedDataSupplier = nil;
NSString *baseAPI = kBaseAPIURL;

@implementation DataSupplier

#pragma mark Delegate Setter
- (void)setDelegate:(id)deleg
{
    delegate = deleg;
}

#pragma mark Fetch Methods
- (void)fetchData;
{
    if (data) {
        // we've already fetched the data from the server and have already stored it.  Just send it back.
        [delegate didFinishGettingData:data];
        return;
    }
    
    
    // if there is already a request in progress, we don't want to interrupt it.
    if (connData) {
        return;
    }
    
    // Show the NetworkActivitySpinner
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES]; 
    
    NSURL *apiURL = [NSURL URLWithString:baseAPI];
    NSURLRequest *request = [NSURLRequest requestWithURL:apiURL];
    [NSURLConnection connectionWithRequest:request delegate:self];
}


#pragma mark -NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)_data
{
    if (!connData) {
        connData = [[[NSMutableData alloc] initWithLength:0] retain];
    }
    [connData appendData:_data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Hide the NetworkActivitySpinner
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO]; 
    
    NSError *parseErr = nil;
    
    JSONDecoder *decoder = [[JSONDecoder alloc] init];
    NSDictionary *results = [decoder objectWithData:connData error:&parseErr];
    if (parseErr) {
        if (delegate) {
            [delegate didFailGettingDataWithError:parseErr];
        }
        return;
    }
    
    NSArray *locations = [results objectForKey:kLocations];
    if (locations && [locations isKindOfClass:[NSArray class]] && [locations count] > 0) {
        data = [[NSArray arrayWithArray:locations] retain];
        if (delegate) {
            [delegate didFinishGettingData:data];
        }
        
    } else {
        if (delegate) {
            NSError *emptyResult = [NSError errorWithDomain:@"Error With Server"
                                                       code:-1
                                                   userInfo:nil];
            [delegate didFailGettingDataWithError:emptyResult];
        } 
    }
    
    
    [connData release];
    connData = nil;
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // Hide the NetworkActivitySpinner
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO]; 
    
    if (delegate) {
        [delegate didFailGettingDataWithError:error];
    }
    
    [connData release];
    connData = nil;
}

#pragma mark Singleton Methods
+ (id)sharedDataSupplier {
    @synchronized(self) {
        if(sharedDataSupplier == nil)
            sharedDataSupplier = [[super allocWithZone:NULL] init];
    }
    return sharedDataSupplier;
}
+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedDataSupplier] retain];
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}
- (id)retain {
    return self;
}
- (unsigned)retainCount {
    return UINT_MAX; //denotes an object that cannot be released
}
- (oneway void)release {
    // never release
}
- (id)autorelease {
    return self;
}
- (id)init {
    if (self = [super init]) {
    }
    return self;
}
- (void)dealloc {
    // Should never be called, but just here for clarity really.
    //[someProperty release];
    [super dealloc];
}
@end
