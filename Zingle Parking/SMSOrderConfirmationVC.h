//
//  SMSOrderConfirmationVC.h
//  zingle restaurants
//
//  Created by Andrew on 6/8/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SMSOrderConfirmationVC : UIViewController {
    id delegate;
    NSDictionary *facility;
    NSString *smsText;
    IBOutlet UIView *topBanner;
    
    IBOutlet UIView *panelVw;
    IBOutlet UITextView *smsTv;
}
@property(nonatomic, retain) id delegate;
@property(nonatomic, retain) NSDictionary *facility;
@property(nonatomic, retain) NSString *smsText;

- (IBAction)back:(id)sender;

@end
