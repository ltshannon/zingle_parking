//
//  FirstViewController.m
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "MapVC.h"
#import "ZPAnnotation.h"
#import "ZPAnnotationView.h"
#import "Constants.h"
#import "DataSupplier.h"

@implementation MapVC
@synthesize map;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDictionary *lastMapRect = [[NSUserDefaults standardUserDefaults] objectForKey:kLastMapRegion];
    if(lastMapRect) {
        MKCoordinateRegion region;
        MKCoordinateSpan span;
        CLLocationCoordinate2D center;
        span.latitudeDelta  = [[lastMapRect objectForKey:kLastMapSpanLat] floatValue];
        span.longitudeDelta = [[lastMapRect objectForKey:kLastMapSpanLng] floatValue];
        center.latitude     = [[lastMapRect objectForKey:kLastMapCenterLat] floatValue];
        center.longitude    = [[lastMapRect objectForKey:kLastMapCenterLng] floatValue];
        
        region.span = span;
        region.center = center;
        [map setRegion:region animated:NO];
    } else {
        cLManager = [[[CLLocationManager alloc] init] retain];
        [cLManager setDelegate:self];
        [cLManager startUpdatingLocation];
    }
    
    
    [[DataSupplier sharedDataSupplier] setDelegate:self];
    [[DataSupplier sharedDataSupplier] fetchData];
    
    //[NSThread detachNewThreadSelector:@selector(loadFacilityData) toTarget:self withObject:nil];
    
    // register for application terminate notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveMapRegion) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveMapRegion) name:UIApplicationWillResignActiveNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
   // self.navigationController.navigationBarHidden = YES;
    
    int mapType = [[[NSUserDefaults standardUserDefaults] objectForKey:kMapTypePref] intValue];
    map.mapType = mapType;
}
-(void)viewWillDisappear:(BOOL)animated
{
    //self.navigationController.navigationBarHidden = NO;
}

#pragma mark - CLLocationDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    [map setCenterCoordinate:newLocation.coordinate animated:YES];
    if (cLManager) {
        [cLManager stopUpdatingLocation];
    }
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errAlert = [[UIAlertView alloc] initWithTitle:@"Location Error" message:@"Your location could not be determined.  Please make sure location services are turned on in the Settings" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [errAlert show];
    [errAlert release];
}

/*
-(void)loadFacilityData
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSString *filepath  = [[NSBundle mainBundle] pathForResource:@"temp" ofType:@"csv"];
    NSError *fileError = nil;
    NSString *csv       = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&fileError];
    if (fileError) {
        NSLog(@"%@", [fileError description]);
    }
    NSArray  *facs      = [csv componentsSeparatedByString:@"\n"];
    
    // create ZPAnnotations
    NSMutableArray *annotations = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSString *str in facs) {
        NSArray *parts = [str componentsSeparatedByString:@","];
        if( [parts count] == 11 ) {
            NSString *primName = [parts objectAtIndex:0];
            NSString *secName   = [parts objectAtIndex:1];
            NSString *zingleNum   = [parts objectAtIndex:2];
            NSString *garageTel = [parts objectAtIndex:3];
            NSString *addr      = [parts objectAtIndex:4];
            NSString *city      = [parts objectAtIndex:5];
            NSString *state     = [parts objectAtIndex:6];
            NSString *zip       = [parts objectAtIndex:7];
            NSString *airport   = [parts objectAtIndex:8];
            float   lat         = [[parts objectAtIndex:9] floatValue];
            float lng           = [[parts objectAtIndex:10] floatValue];
            
            NSArray *objects = [NSArray arrayWithObjects:primName, secName, zingleNum, garageTel, addr, city, state, zip, airport, [NSNumber numberWithFloat:lat], [NSNumber numberWithFloat:lng], nil];
            
            NSArray *keys    = [NSArray arrayWithObjects:@"name", @"secondary_name", @"zingle_number", @"garage_number", @"address", @"city", @"state", @"zip", @"airport", @"latitude", @"longitude", nil];
            NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
            
            
            CLLocationCoordinate2D coord;
            coord.latitude = lat;
            coord.longitude = lng;
            
            ZPAnnotation *annot = [[ZPAnnotation alloc] initWithCoordinate:coord];
            annot.coordinate = coord;
            annot.delegate = self;
            annot.details = dict;
            annot.title = primName;
            annot.subtitle = addr;
            [annotations addObject:annot];
            [annot release];
            
        } else {
            NSLog(@"%i", [parts count]);
        }
    }
    
    [self performSelectorOnMainThread:@selector(finishLoadingFacilityData:) withObject:annotations waitUntilDone: YES];   
    [annotations release];
    [pool drain];
}

-(void)finishLoadingFacilityData:(NSMutableArray*)data
{
    //NSLog(@"Adding %i annotations", [data count]);
    [map addAnnotations:data];
}
*/
#pragma mark -
#pragma mark MKMapView
- (void)mapView:(MKMapView *)_mapView didUpdateUserLocation:(MKUserLocation *)_userLocation {
    
    if(!hasCenteredInitial) {
        [map setCenterCoordinate:map.userLocation.coordinate animated:YES];
        hasCenteredInitial = YES;
    }
}

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views {
    for(MKAnnotationView *annotationView in views) {
        if(annotationView.annotation == mv.userLocation) {
            NSDictionary *lastMapRect = [[NSUserDefaults standardUserDefaults] objectForKey:kLastMapRegion];
            if (!lastMapRect) {
                // center the map on the users location unless they have a saved map region from a previous session
                MKCoordinateRegion region;
                MKCoordinateSpan span;
                
                span.latitudeDelta=0.05;
                span.longitudeDelta=0.05;
                
                CLLocationCoordinate2D location = mv.userLocation.coordinate;
 
                region.span=span;
                region.center=location;
                
                [mv setRegion:region animated:TRUE];
                [mv regionThatFits:region];
            }
        }
    }
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([[annotation class] isEqual:[ZPAnnotation class]]) {
        NSString* identifier = @"ZP";
        ZPAnnotationView *zpAnnotationView = (ZPAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if(!zpAnnotationView)
        {
            zpAnnotationView = [[[ZPAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier] autorelease];	
        }
        zpAnnotationView.annotation = annotation;
        
        return zpAnnotationView;
    } else {
        //NSString *identifier = @"User Location";
    }
    return nil;
}

-(void)saveMapRegion
{
    NSDictionary *lastRegion = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:map.region.center.latitude], kLastMapCenterLat, [NSNumber numberWithFloat:map.region.center.longitude], kLastMapCenterLng, [NSNumber numberWithFloat:map.region.span.latitudeDelta], kLastMapSpanLat, [NSNumber numberWithFloat:map.region.span.longitudeDelta], kLastMapSpanLng, nil];
    [[NSUserDefaults standardUserDefaults] setObject:lastRegion forKey:kLastMapRegion];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -
#pragma mark UISearchBar
- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	forwardGeocoderSearch = [[BSForwardGeocoder alloc] initWithDelegate:self];
	forwardGeocoderSearch.setRegion = YES;
    [forwardGeocoderSearch retain];
	// Forward geocode!
	[forwardGeocoderSearch findLocation:searchBar.text];
	[searchBar setShowsCancelButton:NO animated:YES];
	[searchBar resignFirstResponder];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
	return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
	[searchBar setShowsCancelButton:NO animated:YES];
}

#pragma mark -
#pragma mark BSForwardGeocoderDelegate
-(void)forwardGeocoderError:(NSString *)errorMessage
{
    
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" 
													message:errorMessage
												   delegate:nil 
										  cancelButtonTitle:@"OK" 
										  otherButtonTitles: nil];
	[alert show];
	[alert release];
	[forwardGeocoderSearch release];
    forwardGeocoderSearch = nil;
}
-(void)forwardGeocoderFoundLocation:(BSForwardGeocoder*)geocoder
{
	if(geocoder.status == G_GEO_SUCCESS)
	{
        // REMOVE previous flag pin
        /*
        for (PMSearchAnnotation* annot in mapView.annotations) {
            if ([[annot class] isEqual:[PMSearchAnnotation class]]) {
                [mapView removeAnnotation:annot];
            }
        }
        */
		BSKmlResult *place = [geocoder.results objectAtIndex:0];
        
        // DROP a Flag Pin
        /*
        PMSearchAnnotation *annot = [[PMSearchAnnotation alloc] initWithCoordinate:place.coordinate];
        [mapView addAnnotation:annot];
        [annot release];
        */
                
		[map setRegion:place.coordinateRegion animated:YES];
	}
	else {
		NSString *message = @"";
		
		switch (geocoder.status) {
			case G_GEO_BAD_KEY:
				message = @"The API key is invalid.";
				break;
				
			case G_GEO_UNKNOWN_ADDRESS:
				message = [NSString stringWithFormat:@"Could not find %@", geocoder.searchQuery];
				break;
				
			case G_GEO_TOO_MANY_QUERIES:
				message = @"Too many queries has been made for this API key.";
				break;
				
			case G_GEO_SERVER_ERROR:
				message = @"Server error, please try again.";
				break;
				
				
			default:
				break;
		}
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" 
														message:message
													   delegate:nil 
											  cancelButtonTitle:@"OK" 
											  otherButtonTitles: nil];
		[alert show];
		[alert release];
	}
	
	[forwardGeocoderSearch release];
    forwardGeocoderSearch = nil;
}

#pragma mark -
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}
- (void)viewDidUnload
{
    [map release];
    map = nil;
    [searchB release];
    searchB = nil;
    [super viewDidUnload];

    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)dealloc
{
    if (cLManager) {
        [cLManager stopUpdatingLocation];
        [cLManager release];
        cLManager = nil;
    }
    [map release];
    [searchB release];
    [super dealloc];
}

- (IBAction)back:(id)sender {
    [self saveMapRegion];
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)centerOnUsersLocation:(id)sender {
    if (!cLManager) {
        cLManager = [[[CLLocationManager alloc] init] retain];
        [cLManager setDelegate:self];
    }
    [cLManager startUpdatingLocation];
}

#pragma mark - Data Supplier Delegate
- (void)didFinishGettingData:(NSArray*)data
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                      // parse the data results and make annotations from them
                       NSMutableArray *annotations = [[NSMutableArray alloc] initWithCapacity:[data count]];
                       
                       for (NSDictionary *facility in data) {
                           CLLocationCoordinate2D coord;
                           coord.latitude = [[facility objectForKey:kFacLat] floatValue];
                           coord.longitude = [[facility objectForKey:kFacLng] floatValue];
                           
                           ZPAnnotation *annot = [[ZPAnnotation alloc] initWithCoordinate:coord];
                           annot.coordinate = coord;
                           annot.delegate = self;
                           annot.details = facility;
                           annot.title = [[facility objectForKey:kFacName] stringByReplacingOccurrencesOfString:@"\\'" withString:@"'"];
                           annot.subtitle = [[facility objectForKey:kFacAddress] objectForKey:kFacAddressStreet];
                           [annotations addObject:annot];
                           [annot release];
                       }
                       
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [map addAnnotations:annotations]; 
                                          [annotations release];
                                      });
                   });
    
    //NSLog(@"Did finish getting data from server");
}
- (void)didFailGettingDataWithError:(NSError*)error
{
    UIAlertView *errAlert = [[[UIAlertView alloc] initWithTitle:@"Server Error:"
                                                        message:@"There was an error loading data from the server.  Would you like to try again?"
                                                       delegate:self
                                              cancelButtonTitle:@"No Thanks"
                                              otherButtonTitles:@"Yes", nil] autorelease];
    [errAlert show];
    NSLog(@"Error getting data from server -> %@", [error description]);
}

#pragma mark -UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([alertView cancelButtonIndex] != buttonIndex) {
        // the user wishes to try data download again
        
        [[DataSupplier sharedDataSupplier] setDelegate:self];
        [[DataSupplier sharedDataSupplier] fetchData];
    }
}

@end
