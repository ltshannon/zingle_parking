//
//  NSDictionary+RowFormat.m
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "NSDictionary+RowFormat.h"


@implementation NSMutableDictionary (NSDictionary_RowFormat)

+(NSMutableDictionary*)dictWithCellText:(NSString*)str andImageName:(NSString*)imgName
{
    return [NSMutableDictionary dictWithCellText:str andSubtext:@"" andImageName:imgName];
}

+(NSMutableDictionary*)dictWithCellText:(NSString*)str andSubtext:(NSString*)subtxt andImageName:(NSString*)imgName 
{
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:str, @"text", subtxt, @"subtext", [UIImage imageNamed:imgName], @"image", nil];
}

@end
