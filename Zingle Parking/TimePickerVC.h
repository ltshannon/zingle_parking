//
//  TimePickerVC.h
//  Zingle Parking
//
//  Created by Andrew on 5/26/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TimePickerVC : UIViewController <UIPickerViewDelegate>
{
    
    id delegate;
    
    IBOutlet UIDatePicker *timePicker;
    
}
@property(nonatomic, retain) id delegate;
@property(nonatomic, retain) NSDate *selectedDateTime;

- (IBAction)back:(id)sender;
- (IBAction)continueRequest:(id)sender;
- (IBAction)dateSeleted:(id)sender;
@end
