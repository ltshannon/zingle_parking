//
//  MyGarageDataEntryVC.h
//  Zingle Parking
//
//  Created by Andrew on 7/15/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyGarageDataEntryVC : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    
    NSMutableDictionary *myGarage;
    
    IBOutlet UITableView *myTable;
    NSIndexPath *selectedIndexPath;
    
}
@property(nonatomic, retain) NSMutableDictionary *myGarage;

- (IBAction)back:(id)sender;

@end
