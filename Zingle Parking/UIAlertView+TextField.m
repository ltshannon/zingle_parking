//
//  UIAlertView+TextField.m
//  Zingle Parking
//
//  Created by Andrew on 5/25/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "UIAlertView+TextField.h"


@implementation UIAlertView (UIAlertView_TextField)

-(void)addTextFieldWithMessage:(NSString*)message protected:(BOOL)secured
{  
    
    /*
    UILabel *passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(12,40,260,25)];
    passwordLabel.font = [UIFont systemFontOfSize:16];
    passwordLabel.textColor = [UIColor whiteColor];
    passwordLabel.backgroundColor = [UIColor clearColor];
    passwordLabel.shadowColor = [UIColor blackColor];
    passwordLabel.shadowOffset = CGSizeMake(0,-1);
    passwordLabel.textAlignment = UITextAlignmentCenter;
    passwordLabel.text = message;
    [self addSubview:passwordLabel];
    */
    
    UITextField *passwordField = [[UITextField alloc] initWithFrame:CGRectMake(16,43,252,25)];
    passwordField.text = message;
    passwordField.tag  = 2;
    passwordField.secureTextEntry = secured;
    passwordField.keyboardAppearance = UIKeyboardAppearanceAlert;
    passwordField.delegate = self.delegate;
    passwordField.borderStyle = UITextBorderStyleRoundedRect;
    [passwordField becomeFirstResponder];
    [self addSubview:passwordField];
    
   // [self setTransform:CGAffineTransformMakeTranslation(0,109)];
}

@end
