//
//  LimitedMapView.m
//  Zingle Parking
//
//  Created by Andrew on 6/17/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "LimitedMapView.h"


@implementation LimitedMapView

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    
    if([super respondsToSelector:@selector(scrollViewDidZoom:)]) {
        [super scrollViewDidZoom:scrollView];
    }
    
    if ([self region].span.latitudeDelta > 0.1 || [self region].span.longitudeDelta > 0.1) {
        
        CLLocationCoordinate2D center = self.centerCoordinate;
        MKCoordinateSpan span = MKCoordinateSpanMake(0.05, 0.05);
        
        self.region = MKCoordinateRegionMake(center, span);
        
    }
}

@end
