//
//  NSDictionary+RowFormat.h
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSMutableDictionary (NSDictionary_RowFormat)

+(NSMutableDictionary*)dictWithCellText:(NSString*)str andImageName:(NSString*)imgName;
+(NSMutableDictionary*)dictWithCellText:(NSString*)str andSubtext:(NSString*)subtxt andImageName:(NSString*)imgName;

@end
