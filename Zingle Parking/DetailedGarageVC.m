//
//  DetailedGarageVC.m
//  Zingle Parking
//
//  Created by Andrew on 5/12/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "DetailedGarageVC.h"
#import <QuartzCore/QuartzCore.h>
#import "TimePickerVC.h"
#import "SMSOrderConfirmationVC.h"
#import "Constants.h"
#import "MyGarageDataEntryVC.h"
#import "UIAlertView+TextField.h"


@implementation DetailedGarageVC
@synthesize garage;

// LTS 10-3-2013
NSMutableString *bodyText;

- (void)dealloc
{
    [garage release];
    [panelVw release];
    [favoritesBtn release];
    [detailsAttrVw release];
    [webView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[panelVw layer] setCornerRadius:20];
    [[panelVw layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[panelVw layer] setBorderWidth:1];
    
    NSMutableString *text = [[NSMutableString alloc] initWithCapacity:0];
    
    [text appendString:@"<html>"];
    [text appendString:@"<head><style>body {font-family: Ariel;}</style></head>"];
    [text appendString:@"<body>"];
    [text appendFormat:@"<b>%@</b><p>", [[garage objectForKey:kFacName] stringByReplacingOccurrencesOfString:@"\\'" withString:@"'"]];
     
    if ([garage objectForKey:kFacNameSecondary] && [[garage objectForKey:kFacNameSecondary] length] > 0) {
        [text appendFormat:@"<b>%@</b><br/>", [garage objectForKey:kFacNameSecondary]];
    }
    
    
    [text appendFormat:@"<b>Address:</b> %@<br/>%@, %@ %@<br/>", [[garage objectForKey:kFacAddress] objectForKey:kFacAddressStreet], [[garage objectForKey:kFacAddress] objectForKey:kFacAddressCity], [[garage objectForKey:kFacAddress] objectForKey:kFacAddressState], [[garage objectForKey:kFacAddress] objectForKey:kFacAddressZip]];
    if ([garage objectForKey:kFacPhone] && [[garage objectForKey:kFacPhone] length] > 0) {
        [text appendFormat:@"<b>Phone:</b> %@<br/>", [garage objectForKey:kFacPhone]];
    }
    
    if ([garage objectForKey:kFacWebsite] && [[garage objectForKey:kFacWebsite] length] > 0)
    {
        [text appendFormat:@"<a href=\"%@\">View Website</a><br/>", [garage objectForKey:kFacWebsite]];
    }
     
    [text appendFormat:@"<b>Zingle #:</b> %@<br/>", [garage objectForKey:kFacZingle]];

    [text appendString:@"</body>"];
    [text appendString:@"</html>"];
    
    NSLog(@"Text : %@", text);
    
    [webView loadHTMLString:text baseURL:nil];
    webView.delegate = self;

/*
    NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
    NSAttributedString *string = [[NSAttributedString alloc] initWithHTML:data documentAttributes:NULL];
    
    NSLog(@"String : %@", string);
    
    // set width, height is calculated later from text
    [DTAttributedTextContentView setLayerClass:nil];
    
    [detailsAttrVw setAttributedString:string];
    [string release];
*/
    [text release];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    
    if ( inType == UIWebViewNavigationTypeLinkClicked )
    {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    return YES;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    /* This code works just fine for showing the Add/Remove buttons, however due to changing
     the underlying data structure and storage, previous beta versions of this app may not work
     properly.  Simply delete the app from your phone or simulator and reinstall it.
     */
    if ([self isGarageInFavorites] != -1) {
        [favoritesBtn setTitle:@"REMOVE FROM MY LOCATIONS" forState:UIControlStateNormal];
    } else {
        [favoritesBtn setTitle:@"ADD TO MY LOCATIONS" forState:UIControlStateNormal];
    }
}

- (void)viewDidUnload
{
    [panelVw release];
    panelVw = nil;
    [favoritesBtn release];
    favoritesBtn = nil;
    [detailsAttrVw release];
    detailsAttrVw = nil;
    [webView release];
    webView = nil;
    [super viewDidUnload];
    
    [garage release];
    garage = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// returns -1 if it isn't
-(int)isGarageInFavorites
{
    NSArray *garages = [[NSUserDefaults standardUserDefaults] objectForKey:kFavoriteGarages];
    
    NSString *g1Zingle = [garage objectForKey:kFacZingle];
    
    for (int i=0; i < [garages count]; i++) {
        NSDictionary *gar = [garages objectAtIndex:i];
        NSString *g2Zingle = [gar objectForKey:kFacZingle];
        
        if ([g1Zingle isEqualToString:g2Zingle]) {
            return i;
        }
    }
    return -1;
}

#pragma mark -
#pragma mark Actions
- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)getNow {
    [self composeSMSMessageWithGetTime:nil andValet:nil];
}
-(IBAction)getAt {
    TimePickerVC *timeVC = [[TimePickerVC alloc] initWithNibName:@"TimePickerVC" bundle:nil];
    timeVC.delegate = self;
    timeVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:timeVC animated:YES];
    [timeVC release];
}

- (IBAction)addRemoveToLocations:(id)sender {
    int indexOfGarFavorites = [self isGarageInFavorites];
    if (indexOfGarFavorites != -1) {
        // remove it from favorites.
        NSMutableArray *garages = [[[NSUserDefaults standardUserDefaults] objectForKey:kFavoriteGarages] mutableCopy];
        if (!garages) {
            return;
        }

        [garages removeObjectAtIndex:indexOfGarFavorites];
        [[NSUserDefaults standardUserDefaults] setObject:garages forKey:kFavoriteGarages];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [garages release];
        
// LTS 11/4/2013 added
        [favoritesBtn setTitle:@"ADD TO MY LOCATIONS" forState:UIControlStateNormal];

    }
    else {
        // Add it to favorites.
       
        
        // ask user if they want to save a recurring valet number and/or parking spot number for this garage
        UIAlertView *addDataAlert = [[UIAlertView alloc] initWithTitle:@"Add Valet or Spot Number?"
                                                               message:@"Do you have a recurring valet number or a parking spot number that you'd like to add to this garage?  You may add/edit this later as well."
                                                              delegate:self
                                                     cancelButtonTitle:@"No"
                                                     otherButtonTitles:@"Yes", nil];
        [addDataAlert show];
        [addDataAlert release];
        
// LTS 11/4/2013 added
        [favoritesBtn setTitle:@"REMOVE FROM MY LOCATIONS" forState:UIControlStateNormal];
        
    }
    
// LTS 11/4/2013 added
    [favoritesBtn setNeedsDisplay];

// LTS 11/4/2013 removed
//    if ([self isGarageInFavorites]) {
//        [favoritesBtn setTitle:@"REMOVE FROM MY LOCATIONS" forState:UIControlStateNormal];
//    } else {
//        [favoritesBtn setTitle:@"ADD TO MY LOCATIONS" forState:UIControlStateNormal];
//    }
}

#pragma mark -
#pragma mark MFMessageDelegate
-(void)composeSMSMessageWithGetTime:(NSDate*)getTime andValet:(NSString*)valetStr
{
    if (getTime && !_storedGetTime) {
        [self dismissModalViewControllerAnimated:NO];
    }
    
    // check to see if this garage has a recurring valet or monthly parking spot number.
    NSString *recurringValet = nil;
    NSString *montlyParking = nil;
    int indexOfGarageInFavorites = [self isGarageInFavorites];
    if (indexOfGarageInFavorites != -1) {
        // it is in the favorites list.
        NSMutableArray *garages = [[[NSUserDefaults standardUserDefaults] objectForKey:kFavoriteGarages] mutableCopy];
        if (garages) {
            NSDictionary *gar = [garages objectAtIndex:indexOfGarageInFavorites];
            recurringValet = [gar objectForKey:@"recurring_valet_number"];
            montlyParking = [gar objectForKey:@"parking_spot_number"];
            
            if( recurringValet && [recurringValet length] < 1) {
                recurringValet = nil;
            }
            
            if( montlyParking && [montlyParking length] < 1) {
                montlyParking = nil;
            }
            
            if (!recurringValet && !montlyParking && !valetStr) {
                [_storedGetTime release];
                _storedGetTime = nil;
                _storedGetTime = getTime;
                [_storedGetTime retain];
                UIAlertView *entryAlert = [[UIAlertView alloc] initWithTitle:@"Valet Ticket:"
                                                                     message:@"\n"
                                                                    delegate:self
                                                           cancelButtonTitle:@"Ok"
                                                           otherButtonTitles:nil];

// LTS 10-2-2013
//                [entryAlert addTextFieldWithMessage:@"" protected:NO];
                
                entryAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
                [entryAlert textFieldAtIndex:0].delegate = (id)self;

                [entryAlert show];
                [entryAlert release]; 
                return;
            }
        }
    } else if (!valetStr) {
        [_storedGetTime release];
        _storedGetTime = nil;
        _storedGetTime = getTime;
        [_storedGetTime retain];
        UIAlertView *entryAlert = [[UIAlertView alloc] initWithTitle:@"Valet Ticket:"
                                                             message:@"\n"
                                                            delegate:self
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];

// LTS 10-2-2013
//        [entryAlert addTextFieldWithMessage:@"" protected:NO];
        entryAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [entryAlert textFieldAtIndex:0].delegate = (id)self;

        [entryAlert show];
        [entryAlert release]; 
        return;
    }
    
    
    // load profile from disk if available
    NSDictionary *userData = [[NSUserDefaults standardUserDefaults] objectForKey:kUserZingleProfile];
    
    NSString *fName = [userData objectForKey:@"First Name"];
    NSString *lName = [userData objectForKey:@"Last Name"];
    
  ///  NSString *vTicket = [userData objectForKey:@"Valet Ticket"];
    
    NSString *cType   = [userData objectForKey:@"Car Type"];
    NSString *cColor  = [userData objectForKey:@"Car Color"];
    NSString *cLicense = [userData objectForKey:@"License Plate"];
    
    NSString *phone  = [userData objectForKey:@"Contact Number"];
    
 ///   NSString *pSpotNum = [userData objectForKey:@"Parking Spot Number"];
 ///   NSString *rValetNum = [userData objectForKey:@"Recurring Valet Number"];
    
// LTS 10-3-2013
//    NSMutableString *bodyText = [[NSMutableString alloc] initWithCapacity:0];
    bodyText = [[NSMutableString alloc] initWithCapacity:0];

    if (getTime)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        NSString *dateString = [formatter stringFromDate:getTime];
        [formatter release];
        [bodyText appendFormat:@"Please be ready @ %@ \n", dateString];
    } else
    {
        [bodyText appendString:@"Please be ready ASAP. \n"];
    }
    if (valetStr)
    {
        [bodyText appendFormat:@"Valet Ticket: %@ \n", valetStr];
    }
    
    if (recurringValet)
    {
        [bodyText appendFormat:@"Recurring Valet: %@ \n", recurringValet];
    }
    if (montlyParking)
    {
        [bodyText appendFormat:@"Parking Spot #: %@ \n", montlyParking];
    }
    
    if ([fName length] > 0 || [lName length] > 0)
    {
        [bodyText appendFormat:@"Name: %@ %@ \n", fName, lName];
    }

    if ([cType length] > 0 || [cColor length] > 0 || [cLicense length] > 0)
    {
        [bodyText appendFormat:@"Car: %@ %@ %@ \n", cType, cColor, cLicense];
    }
    if ([phone length] > 0) {
        [bodyText appendFormat:@"Phone: %@ \n", phone];
    }
   // NSLog(@"%@", bodyText);
    
    
    
    MFMessageComposeViewController *controller = [[[MFMessageComposeViewController alloc] init] autorelease];
	if([MFMessageComposeViewController canSendText])
	{
		controller.body = bodyText;
        
        NSString *zingleNumber = [[garage objectForKey:kFacZingle] stringByReplacingOccurrencesOfString:@"." withString:@""];
        zingleNumber = [zingleNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
        zingleNumber = [zingleNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
        zingleNumber = [zingleNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
        zingleNumber = [zingleNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        
		controller.recipients = [NSArray arrayWithObjects:zingleNumber, nil];
		controller.messageComposeDelegate = self;
        controller.delegate = self;
		[self presentModalViewController:controller animated:NO];
	}/* else { //device isn't capable of sending SMS messages
#warning DISABLE THIS CODE BEFORE PRODUCTION!
        
         // The code contained within the else statement should be disabled before submitting to the App Store.
		// it allows devices without SMS capabilities (iPods and simulator) to see the SMS confirmation screen.
         
        SMSOrderConfirmationVC *viewController = [[SMSOrderConfirmationVC alloc] initWithNibName:@"SMSOrderConfirmationVC" bundle:nil];
        viewController.delegate = self;
        viewController.smsText = bodyText;
        viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentModalViewController:viewController animated:YES];
        [viewController release];
    }*/
    
//    [bodyText release];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissModalViewControllerAnimated:NO];
    
	switch (result) {
		case MessageComposeResultCancelled:;
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:;
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Zingle Parking" message:@"Unknown Error"
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
			[alert release];
			break;
		case MessageComposeResultSent:;
            SMSOrderConfirmationVC *viewController = [[SMSOrderConfirmationVC alloc] initWithNibName:@"SMSOrderConfirmationVC" bundle:nil];
            viewController.delegate = self;

// LTS 10-3-2013
//            viewController.smsText = controller.body;
            viewController.smsText = bodyText;
            viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentModalViewController:viewController animated:YES];
            [viewController release];
			break;
		default:
			break;
	}
    [bodyText release];
}

#pragma mark -
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([[alertView title] isEqualToString:@"Valet Ticket:"])
    {
// LTS 10-2-2013
//        UITextField* tf = (UITextField*)[alertView viewWithTag:2];
        UITextField *tf = [alertView textFieldAtIndex:0];
        if (tf) {
            [self composeSMSMessageWithGetTime:_storedGetTime andValet:tf.text];
        }
        
    } else {
        NSMutableArray *garages = [[[NSUserDefaults standardUserDefaults] objectForKey:kFavoriteGarages] mutableCopy];
        if (!garages) {
            garages = [[NSMutableArray alloc] initWithCapacity:0];
        }
        
        [garages addObject:garage];
        
        [[NSUserDefaults standardUserDefaults] setObject:garages forKey:kFavoriteGarages];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [garages release];
        
        if ([alertView cancelButtonIndex] == buttonIndex) {
            // user declined to add more info to this garage like recurring valet number or parking spot number
            
        } else {
            // user wants to add more details.
            MyGarageDataEntryVC *viewController = [[MyGarageDataEntryVC alloc] initWithNibName:@"MyGarageDataEntryVC" bundle:nil];
            viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            viewController.myGarage = [garage mutableCopy];
            [self presentModalViewController:viewController animated:YES];
            [viewController release];
        }
    }
}

@end
