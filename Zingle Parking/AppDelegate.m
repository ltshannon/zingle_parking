//
//  Zingle_ParkingAppDelegate.m
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "AppDelegate.h"
#import "UIAlertView+TextField.h"
#import "FacebookVC.h"
#import "Constants.h"

@implementation AppDelegate
@synthesize menuVC, fbController;
@synthesize window=_window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    BOOL pinRequired = [[NSUserDefaults standardUserDefaults] boolForKey:kPinRequired];
    if (pinRequired) {
        [self showPinAlert];
    } else {
        [self showZingleMenu];
    }
    
    [[DataSupplier sharedDataSupplier] setDelegate:self];
    [[DataSupplier sharedDataSupplier] fetchData];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    BOOL pinRequired = [[NSUserDefaults standardUserDefaults] boolForKey:kPinRequired];
    if (pinRequired) {
        [self showPinAlert];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [[((FacebookVC*)fbController) facebook] handleOpenURL:url];
}

- (void)dealloc
{
    [menuVC release];
    [_window release];
    [fbController release];
    [super dealloc];
}

#pragma mark -
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Ok"])
    {
// LTS 11/24/2013 fix for bug APPS-15 & APPS 22
//        UITextField* tf = (UITextField*)[alertView viewWithTag:2];
        UITextField *tf = [alertView textFieldAtIndex:0];

        if (tf) {
            NSString *pin = [[NSUserDefaults standardUserDefaults] objectForKey:kPin];
            if ([pin isEqualToString:tf.text]) {
                [self showZingleMenu];
            } else {
                [self showPinAlert];
            }
        }
    }
}

#pragma mark -
-(void)showPinAlert
{
    UIAlertView *entryAlert = [[UIAlertView alloc] initWithTitle:@"Enter Password:"
                                                         message:@"\n"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];

// LTS 11/24/2013 fix for bug APPS-15 & APPS 22
//        [entryAlert addTextFieldWithMessage:@"" protected:YES];
    entryAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
    [entryAlert textFieldAtIndex:0].delegate = (id)self;
    
    [entryAlert show];
    [entryAlert release];
}
-(void)showZingleMenu
{
    // Add the tab bar controller's current view as a subview of the window
    self.window.rootViewController = self.menuVC;
    [self.window makeKeyAndVisible];
}

-(void)goBackToHomeScreen
{
    [self.window.rootViewController dismissModalViewControllerAnimated:YES];
}

#pragma mark - Data Supplier Delegate
- (void)didFinishGettingData:(NSArray*)data
{
    //NSLog(@"Did finish getting data from server");
}
- (void)didFailGettingDataWithError:(NSError*)error
{
    NSLog(@"Error getting data from server -> %@", [error description]);
}

@end
