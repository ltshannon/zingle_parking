//
//  LimitedMapView.h
//  Zingle Parking
//
//  Created by Andrew on 6/17/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mapkit/Mapkit.h>
#import "MKMapView+Scrolling.h"

@interface LimitedMapView : MKMapView <UIScrollViewDelegate> {
    
}

@end
