//
//  MyGaragesVC.h
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyGaragesVC : UIViewController <UITableViewDelegate, UITableViewDataSource> {

    NSMutableArray *garages;
    
    IBOutlet UITableView *myTable;
    IBOutlet UIButton *editBtn;
    id profileDelegate;
}
@property(nonatomic, retain) id profileDelegate;

- (IBAction)back:(id)sender;
- (IBAction)editMyGarages:(id)sender;
@end
