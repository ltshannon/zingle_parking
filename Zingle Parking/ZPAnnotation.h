//
//  ZPAnnotation.h
//  Zingle Parking
//
//  Created by Andrew on 5/12/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mapkit/Mapkit.h>

@interface ZPAnnotation : NSObject <MKAnnotation> {
    CLLocationCoordinate2D _coordinate;
    id _delegate;
    NSString *title, *subtitle;
    NSDictionary *_details;
}

@property(nonatomic, retain) NSString *title, *subtitle;
@property (nonatomic, assign) id delegate;
@property (nonatomic, retain) NSDictionary *details;

-(id)initWithCoordinate:(CLLocationCoordinate2D)coord;
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;
- (CLLocationCoordinate2D)getCoordinate;

@end
