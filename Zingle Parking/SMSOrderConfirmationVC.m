//
//  SMSOrderConfirmationVC.m
//  zingle restaurants
//
//  Created by Andrew on 6/8/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "SMSOrderConfirmationVC.h"
#import <QuartzCore/QuartzCore.h>

@implementation SMSOrderConfirmationVC
@synthesize delegate, facility, smsText;

- (void)dealloc
{
    [delegate release];
    [facility release];
    [smsText release];
    [smsTv release];
    [topBanner release];
    [panelVw release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[topBanner layer] setShadowColor:[[UIColor blackColor] CGColor]];
    [[topBanner layer] setShadowOffset:CGSizeMake(0, 1)];
    [[topBanner layer] setShadowOpacity:0.5];
    
    [[panelVw layer] setCornerRadius:20];
    [[panelVw layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[panelVw layer] setBorderWidth:1];
    
    smsTv.text = smsText;
}

- (void)viewDidUnload
{
    [smsTv release];
    smsTv = nil;
    [topBanner release];
    topBanner = nil;
    [panelVw release];
    panelVw = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

@end
