//
//  MyGarageDataEntryVC.m
//  Zingle Parking
//
//  Created by Andrew on 7/15/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "MyGarageDataEntryVC.h"
#import "UIAlertView+TextField.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

@implementation MyGarageDataEntryVC
@synthesize myGarage;

- (void)dealloc
{
    [myTable release];
    [myGarage release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[myTable layer] setCornerRadius:20];
    [[myTable layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[myTable layer] setBorderWidth:1];
    
    /*
    NSArray *section3 = [NSMutableArray arrayWithObjects:
                         [NSMutableDictionary dictWithCellText:@"Parking Spot Number"
                                                    andSubtext:[userData objectForKey:@"Parking Spot Number"]
                                                  andImageName:@"28-star.png"], 
                         [NSMutableDictionary dictWithCellText:@"Recurring Valet Number"
                                                    andSubtext:[userData objectForKey:@"Recurring Valet Number"]
                                                  andImageName:@"02-redo.png"], nil];
     */
}

- (void)viewDidUnload
{
    [myTable release];
    myTable = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        UIImageView *imgVw = [[UIImageView alloc] initWithFrame:CGRectMake(15, 2, 40, 40)];
        imgVw.tag = 1;
        [imgVw setContentMode:UIViewContentModeLeft];
        [cell addSubview:imgVw];
        [imgVw release];
        
        UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(55, 3, cell.frame.size.width-62, 20)];
        [text setBackgroundColor:[UIColor clearColor]];
        [text setFont:[UIFont boldSystemFontOfSize:17]];
        text.tag = 2;
        [cell addSubview:text];
        [text release];
        
        UILabel *subtext = [[UILabel alloc] initWithFrame:CGRectMake(55, 25, cell.frame.size.width-62, 20)];
        [subtext setBackgroundColor:[UIColor clearColor]];
        [subtext setFont:[UIFont systemFontOfSize:15]];
        [subtext setTextColor:[UIColor darkGrayColor]];
        subtext.tag = 3;
        [cell addSubview:subtext];
        [subtext release];
    }

    
    UIImage *img = nil;
    NSString *titleText, *subTitleText = nil;
    if (indexPath.row == 0) {
        img = [UIImage imageNamed:@"02-redo.png"];
        titleText = @"Recurring Valet Number";
        subTitleText = [myGarage objectForKey:@"recurring_valet_number"];
    } else {
        img = [UIImage imageNamed:@"28-star.png"];
        titleText = @"Parking Spot Number";
        subTitleText = [myGarage objectForKey:@"parking_spot_number"];
    }
    
    UIImageView *imgVw = (UIImageView*)[cell viewWithTag:1];
    if (imgVw) {
        imgVw.image = img;
    }
    
    UILabel *text = (UILabel*)[cell viewWithTag:2];
    if (text) {
        text.text = titleText;
    }
    
    UILabel *subtext = (UILabel*)[cell viewWithTag:3];
    if (subtext) {
        subtext.text = subTitleText;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    if (selectedIndexPath) {
        [selectedIndexPath release];
        selectedIndexPath = nil;
    }
    selectedIndexPath = [indexPath copy];

    UIAlertView *entryAlert = nil;
    NSString *rowValue = @"";
    
    if (indexPath.row == 0) {
        // it's the recurring valet number
        entryAlert = [[UIAlertView alloc] initWithTitle:@"Recurring Valet #:"
                                                message:@"\n"
                                               delegate:self
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil];
    } else {
        // it's the monthly parking spot
        entryAlert = [[UIAlertView alloc] initWithTitle:@"Monthly Parking Spot:"
                                                message:@"\n"
                                               delegate:self
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil];
    }

// LTS 11/4/2013 Removed & added
//    [entryAlert addTextFieldWithMessage:rowValue protected:NO];
    entryAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [entryAlert textFieldAtIndex:0].delegate = (id)self;

    [entryAlert show];
    [entryAlert release]; 
}

- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Ok"])
    {
// LTS 11/4/2013 Removed & added
//        UITextField* tf = (UITextField*)[alertView viewWithTag:2];
        UITextField *tf = [alertView textFieldAtIndex:0];

        if (tf && selectedIndexPath) {
            if (selectedIndexPath.row == 0) {
                [myGarage setObject:tf.text forKey:@"recurring_valet_number"];
            } else if(selectedIndexPath.row == 1) {
                [myGarage setObject:tf.text forKey:@"parking_spot_number"];
            }

            NSMutableArray *garages = [[[NSUserDefaults standardUserDefaults] objectForKey:kFavoriteGarages] mutableCopy];
            if (!garages) {
                garages = [[NSMutableArray alloc] initWithCapacity:0];
            }
            
            
            NSString *g1Zingle = [myGarage objectForKey:kFacZingle];
            NSDictionary *garageToRemove = nil;
            for (NSDictionary *gar in garages) {
                NSString *g2Zingle = [gar objectForKey:kFacZingle];
                
                if ([g1Zingle isEqualToString:g2Zingle]) {
                    garageToRemove = gar;
                    break;
                }
            }
            if (garageToRemove) {
                [garages removeObject:garageToRemove];
                [garages addObject:myGarage];
                [[NSUserDefaults standardUserDefaults] setObject:garages forKey:kFavoriteGarages];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [garages release];

            [selectedIndexPath release];
            selectedIndexPath = nil;
            [myTable reloadData];
        }
    }
}


@end
