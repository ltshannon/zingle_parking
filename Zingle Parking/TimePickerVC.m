//
//  TimePickerVC.m
//  Zingle Parking
//
//  Created by Andrew on 5/26/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "TimePickerVC.h"
#import "DetailedGarageVC.h"

#define kOneDayInSeconds 86400

@implementation TimePickerVC
@synthesize delegate;

- (void)dealloc
{
    [delegate release];
    [timePicker release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    timePicker = [[UIDatePicker alloc] init];
    [timePicker setDatePickerMode:UIDatePickerModeDateAndTime];
//    [timePicker setMinimumDate:[NSDate dateWithTimeIntervalSinceNow:-(60*5)]];
//    [timePicker setMaximumDate:[NSDate dateWithTimeIntervalSinceNow:2*kOneDayInSeconds]];
//    [timePicker setDate:[NSDate date]];
    
}

// LTS 10-28-2013
- (void) dateSeleted:(id)sender
{

    UIDatePicker *myDatePicker = (UIDatePicker *) sender;
    self.selectedDateTime = myDatePicker.date;

}

- (void)viewDidUnload
{
    [delegate release];
    delegate = nil;
    [timePicker release];
    timePicker = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)continueRequest:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(composeSMSMessageWithGetTime:andValet:)])
    {
        [delegate composeSMSMessageWithGetTime:self.selectedDateTime andValet:nil];
    }
}

@end
