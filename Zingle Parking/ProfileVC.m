//
//  ThirdViewController.m
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "ProfileVC.h"
#import <QuartzCore/QuartzCore.h>
#import "MyGaragesVC.h"
#import "NSDictionary+RowFormat.h"
#import "UIAlertView+TextField.h"
#import "Constants.h"

@implementation ProfileVC

- (void)dealloc
{
    [table release];
    [profile release];
    [userData release];
    [super dealloc];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[table layer] setCornerRadius:20];
    [[table layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[table layer] setBorderWidth:1];
    
    // load profile from disk if available
    userData = [[[NSUserDefaults standardUserDefaults] objectForKey:kUserZingleProfile] mutableCopy];
    
    // else create a new one
    if (!userData) {
        userData = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"", @"Car Type",
                    @"", @"Car Color", @"", @"License Plate", @"", @"First Name", @"", @"Last Name", @"", @"Contact Number", nil];
    }
    [userData retain];

// LTS 10-2-2013
//    NSArray *section1 = [NSMutableArray arrayWithObjects:
    profile = [NSMutableArray arrayWithObjects:
                         [NSMutableDictionary dictWithCellText:@"Car Type"
                                                    andSubtext:[userData objectForKey:@"Car Type"]
                                                  andImageName:@"30-key.png"], 
                         [NSMutableDictionary dictWithCellText:@"Car Color"
                                                    andSubtext:[userData objectForKey:@"Car Color"]
                                                  andImageName:@"98-palette.png"],
                         [NSMutableDictionary dictWithCellText:@"License Plate"
                                                    andSubtext:[userData objectForKey:@"License Plate"]
                                                  andImageName:@"152-rolodex.png"],
// LTS 10-12-2013
//                                                  andImageName:@"152-rolodex.png"], nil];
    
//    NSArray *section2 = [NSMutableArray arrayWithObjects:
                         [NSMutableDictionary dictWithCellText:@"First Name"
                                                    andSubtext:[userData objectForKey:@"First Name"]
                                                  andImageName:@"111-user.png"], 
                         [NSMutableDictionary dictWithCellText:@"Last Name"
                                                    andSubtext:[userData objectForKey:@"Last Name"]
                                                  andImageName:@"123-id-card.png"],
                         [NSMutableDictionary dictWithCellText:@"Contact Number"
                                                    andSubtext:[userData objectForKey:@"Contact Number"]
                                                  andImageName:@"75-phone.png"], nil];

// LTS 10-2-2013
//    profile = [[NSMutableArray alloc] initWithObjects:section1, section2, nil];

    
    [profile retain];
    [table reloadData];
}
- (void)viewDidUnload
{
    [table release];
    table = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
// LTS 10-2-2013
//    return [profile count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!profile || [profile count] < section)
    {
        return 0;
    }

// LTS 10-2-2013
//    NSArray *sectCollection = [profile objectAtIndex:section];
//    return [sectCollection count];
    return [profile count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];

        UIImageView *imgVw = [[UIImageView alloc] initWithFrame:CGRectMake(15, 2, 40, 40)];
        imgVw.tag = 1;
        [imgVw setContentMode:UIViewContentModeLeft];
        [cell addSubview:imgVw];
        [imgVw release];
        
        UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(55, 3, cell.frame.size.width-62, 20)];
        [text setBackgroundColor:[UIColor clearColor]];
        [text setFont:[UIFont boldSystemFontOfSize:17]];
        text.tag = 2;
        [cell addSubview:text];
        [text release];
        
        UILabel *subtext = [[UILabel alloc] initWithFrame:CGRectMake(55, 25, cell.frame.size.width-62, 20)];
        [subtext setBackgroundColor:[UIColor clearColor]];
        [subtext setFont:[UIFont systemFontOfSize:15]];
        [subtext setTextColor:[UIColor darkGrayColor]];
        subtext.tag = 3;
        [cell addSubview:subtext];
        [subtext release];
    }
    
    
// LTS 10-2-2013
//    NSDictionary *row = [[profile objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    NSDictionary *row = [profile objectAtIndex:indexPath.row];
    
    
    UIImageView *imgVw = (UIImageView*)[cell viewWithTag:1];
    if (imgVw) {
        imgVw.image = [row objectForKey:@"image"];
    }
    
    UILabel *text = (UILabel*)[cell viewWithTag:2];
    if (text) {
        text.text = [row objectForKey:@"text"];
    }
    
    UILabel *subtext = (UILabel*)[cell viewWithTag:3];
    if (subtext) {
        subtext.text = [row objectForKey:@"subtext"];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    selectedIndexPath = indexPath;

    // Everything else
// LTS 10-2-2013
//    NSDictionary *row = [[profile objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    NSDictionary *row = [profile objectAtIndex:indexPath.row];
    
    UIAlertView *entryAlert = [[UIAlertView alloc] initWithTitle:[row objectForKey:@"text"]
                                                         message:@"\n"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
    
// LTS 10-2-2013
//    BOOL shouldSecure = NO;
//    [entryAlert addTextFieldWithMessage:[userData objectForKey:[row objectForKey:@"text"]] protected:shouldSecure];
    entryAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    entryAlert.tag = indexPath.row;
    [entryAlert textFieldAtIndex:0].delegate = (id)self;
    
    [entryAlert show];
    [entryAlert release]; 
}

-(void)setGarageName:(NSString*)name
{
    NSMutableDictionary *row = [[profile objectAtIndex:selectedIndexPath.section] objectAtIndex:selectedIndexPath.row];
    [row setObject:name forKey:@"subtext"];
// LTS 1-2-2013
//    [[profile objectAtIndex:selectedIndexPath.section] replaceObjectAtIndex:selectedIndexPath.row withObject:row];
    [profile replaceObjectAtIndex:selectedIndexPath.row withObject:row];
    
    [userData setObject:name forKey:[row objectForKey:@"text"]];
    
    [[NSUserDefaults standardUserDefaults] setObject:userData forKey:kUserZingleProfile];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    selectedIndexPath = nil;
    [table reloadData];
}

- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Ok"]) {
//        UITextField* tf = (UITextField*)[alertView viewWithTag:2];
        UITextField *tf = [alertView textFieldAtIndex:0];

        if (tf && selectedIndexPath)
        {
// LTS 10-2-2013
//            NSMutableDictionary *row = [[profile objectAtIndex:selectedIndexPath.section] objectAtIndex:selectedIndexPath.row];
            NSMutableDictionary *row = [profile objectAtIndex:alertView.tag];
            [row setObject:tf.text forKey:@"subtext"];

// LTS 10-2-2013
//            [[profile objectAtIndex:selectedIndexPath.section] replaceObjectAtIndex:selectedIndexPath.row withObject:row];
            [profile replaceObjectAtIndex:alertView.tag withObject:row];
            
            [userData setObject:tf.text forKey:[row objectForKey:@"text"]];
            
            [[NSUserDefaults standardUserDefaults] setObject:userData forKey:kUserZingleProfile];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            selectedIndexPath = nil;
            [table reloadData];
        }
    }
}


@end
