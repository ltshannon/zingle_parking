//
//  ThirdViewController.h
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProfileVC : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UITableView *table;
    
    NSMutableArray *profile;
    NSMutableDictionary *userData;
    
    NSIndexPath *selectedIndexPath;
}

-(void)setGarageName:(NSString*)name;
- (IBAction)back:(id)sender;

@end
