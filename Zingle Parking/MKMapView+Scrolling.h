//
//  MKMapView+Scrolling.h
//  Zingle Parking
//
//  Created by Andrew on 6/24/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//


/*
 This category is here to shut the compiler warning up for the "Limited MapView" subclass
 */

#import <Foundation/Foundation.h>
#import <Mapkit/Mapkit.h>

@interface MKMapView (MKMapView_Scrolling)

-(void)scrollViewDidZoom:(UIScrollView*)scrollView;

@end
