//
//  SettingsVC.m
//  Zingle Parking
//
//  Created by Andrew on 5/24/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "SettingsVC.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>
#import "UIAlertView+TextField.h"

@implementation SettingsVC

- (void)dealloc
{
    [backVw release];
    [mapTypeSegment release];
    [pinLbl release];
    [pinSwitch release];
    [pinBtn removeObserver:self forKeyPath:@"enabled"];
    [pinBtn release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[backVw layer] setCornerRadius:10];
    [[backVw layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[backVw layer] setBorderWidth:1];
    
    
    [pinBtn addObserver:self forKeyPath:@"enabled" options:NSKeyValueObservingOptionNew context:nil];
    pinSwitch.on = pinBtn.enabled = [[NSUserDefaults standardUserDefaults] boolForKey:kPinRequired];
    
    [self assignCodedPinToLabel];
}
-(void)viewWillAppear:(BOOL)animated
{
    int index = [[[NSUserDefaults standardUserDefaults] objectForKey:kMapTypePref] intValue];
    [mapTypeSegment setSelectedSegmentIndex:index];
}
- (void)viewDidUnload
{
    [backVw release];
    backVw = nil;
    [mapTypeSegment release];
    mapTypeSegment = nil;
    [pinLbl release];
    pinLbl = nil;
    [pinSwitch release];
    pinSwitch = nil;
    [pinBtn removeObserver:self forKeyPath:@"enabled"];
    [pinBtn release];
    pinBtn = nil;
    [super viewDidUnload];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isEqual:pinBtn]) {
        if (pinBtn.enabled) {
            pinBtn.alpha = 1.0;
        } else {
            pinBtn.alpha = 0.5;
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)pickMapType:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:[mapTypeSegment selectedSegmentIndex]] forKey:kMapTypePref];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)togglePinOnOff:(id)sender {
    pinBtn.enabled = pinSwitch.on;
    [[NSUserDefaults standardUserDefaults] setBool:pinSwitch.on forKey:kPinRequired];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (pinSwitch.on) {
        [self changePin:nil];
    }
    
}

- (IBAction)changePin:(id)sender {
    UIAlertView *entryAlert = [[UIAlertView alloc] initWithTitle:@"New Password:"
                                                         message:@"\n"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];

// LTS 11/24/2013 fix for bug APPS-15 & APPS 22
//    [entryAlert addTextFieldWithMessage:pinLbl.text protected:YES];
    entryAlert.alertViewStyle = UIAlertViewStyleSecureTextInput;
    [entryAlert textFieldAtIndex:0].delegate = (id)self;

    [entryAlert show];
    [entryAlert release];
}

#pragma mark -
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Ok"]) {
// LTS 11/24/2013 fix for bug APPS-15 & APPS 22
//        UITextField* tf = (UITextField*)[alertView viewWithTag:2];
        UITextField *tf = [alertView textFieldAtIndex:0];
        if (tf) {
            [[NSUserDefaults standardUserDefaults] setObject:tf.text forKey:kPin];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self assignCodedPinToLabel];
        }
    }
}

-(void)assignCodedPinToLabel
{
    NSString *pin = [[NSUserDefaults standardUserDefaults] objectForKey:kPin];
    int passLength = [pin length];
    int current = 0;
    NSMutableString *secureStr = [[NSMutableString alloc] initWithCapacity:passLength];
    while (current < passLength) {
        current++;
        [secureStr appendString:@"✪"];
    }
    pinLbl.text = secureStr;
    [secureStr release];
}
@end
