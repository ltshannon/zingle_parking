//
//  DataSupplier.h
//  EasyPark
//
//  Created by Andrew Schenk on 7/24/11.
//  Copyright 2011 Mobile Parking Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mapkit/Mapkit.h>

// implement a delegate/protocol structure
@protocol DataSupplierDelegate <NSObject>

@optional


@required
- (void)didFinishGettingData:(NSArray*)data;
- (void)didFailGettingDataWithError:(NSError*)error;
@end


// Interface for DataSupplier Object.  DataSupplier is a Singleton pattern.
@interface DataSupplier : NSObject
{
    id <DataSupplierDelegate> delegate;
    
    @private
    NSArray *data;
    NSMutableData *connData;
}

// Returns a copy of the singleton data supplier
+ (id)sharedDataSupplier;

// Set Delegate
- (void)setDelegate:(id)deleg;

// Fetch data
- (void)fetchData;

@end
