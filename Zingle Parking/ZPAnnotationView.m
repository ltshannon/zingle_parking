//
//  ZPAnnotationView.m
//  Zingle Parking
//
//  Created by Andrew on 5/12/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "ZPAnnotationView.h"
#import "ZPAnnotation.h"
#import "MapVC.h"
#import "DetailedGarageVC.h"

@implementation ZPAnnotationView

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
	if (self != nil) {
        
		self.canShowCallout = YES;
		
		UIButton *detailbtn = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		[detailbtn addTarget:self action:@selector(showDetails:) forControlEvents:UIControlEventTouchUpInside];
		
		self.rightCalloutAccessoryView = detailbtn;
        self.image = [UIImage imageNamed:@"zp_map_pin.png"];
        /*
        [self addObserver:self
               forKeyPath:@"selected"
                  options:NSKeyValueObservingOptionNew
                  context:GMAP_ANNOTATION_SELECTED];
         */
        
	}
	return self;
}


-(void)showDetails:(id)sender
{
	ZPAnnotation * annotation = self.annotation;
	
    
	DetailedGarageVC *controller = [[DetailedGarageVC alloc] initWithNibName:@"DetailedGarageVC" bundle:nil];
	controller.garage = annotation.details;

    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [((MapVC*)annotation.delegate) presentModalViewController:controller animated:YES];
	[controller release];
    
}


@end
