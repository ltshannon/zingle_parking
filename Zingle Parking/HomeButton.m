//
//  HomeButton.m
//  Zingle Parking
//
//  Created by Andrew on 7/20/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "HomeButton.h"
#import "AppDelegate.h"

@implementation HomeButton

-(void)awakeFromNib
{
    [self setImage:[UIImage imageNamed:@"53-house.png"] forState:UIControlStateNormal];
    [self addTarget:self action:@selector(signalAppDelegateToReturnHome) forControlEvents:UIControlEventTouchUpInside];
}

-(void)signalAppDelegateToReturnHome
{
    AppDelegate *delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if ([delegate respondsToSelector:@selector(goBackToHomeScreen)]) {
        [delegate goBackToHomeScreen];
    }
}

@end
