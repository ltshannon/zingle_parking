//
//  ZPAnnotation.m
//  Zingle Parking
//
//  Created by Andrew on 5/12/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "ZPAnnotation.h"


@implementation ZPAnnotation

@synthesize coordinate		= _coordinate;
@synthesize details			= _details;
@synthesize delegate		= _delegate;
@synthesize title, subtitle;


-(id)initWithCoordinate:(CLLocationCoordinate2D)coord
{
    self = [super init];
	if (self) {
		_coordinate = coord;
	}
	return self;
}

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate {
    _coordinate = coordinate;
}

- (CLLocationCoordinate2D)getCoordinate {
	return _coordinate;
}

-(void) dealloc {
    [_details release];
    [title release];
    [subtitle release];
	[super dealloc];
}

@end
