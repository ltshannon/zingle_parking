//
//  UIAlertView+TextField.h
//  Zingle Parking
//
//  Created by Andrew on 5/25/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIAlertView (UIAlertView_TextField)

-(void)addTextFieldWithMessage:(NSString*)message protected:(BOOL)secured;

@end
