//
//  SettingsVC.h
//  Zingle Parking
//
//  Created by Andrew on 5/24/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SettingsVC : UIViewController {
    
    IBOutlet UIView *backVw;
    IBOutlet UISegmentedControl *mapTypeSegment;
    IBOutlet UISwitch *pinSwitch;
    IBOutlet UIButton *pinBtn;
    IBOutlet UILabel *pinLbl;
}
- (IBAction)back:(id)sender;
- (IBAction)pickMapType:(id)sender;
- (IBAction)togglePinOnOff:(id)sender;
- (IBAction)changePin:(id)sender;

-(void)assignCodedPinToLabel;
@end
