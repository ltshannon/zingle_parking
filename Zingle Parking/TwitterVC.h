//
//  TwitterVC.h
//  Zingle Parking
//
//  Created by Andrew on 6/24/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SA_OAuthTwitterController.h"

@class SA_OAuthTwitterEngine;

@interface TwitterVC : UIViewController <SA_OAuthTwitterControllerDelegate> {
    IBOutlet UIView *panelVw;
    SA_OAuthTwitterEngine *engine;
    IBOutlet UITextField *tweetTf;
    IBOutlet UIButton *tweetBtn;
}
- (IBAction)back:(id)sender;
- (IBAction)sendTweet:(id)sender;

@end
