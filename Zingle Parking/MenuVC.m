//
//  zingle_restaurantsViewController.m
//  zingle restaurants
//
//  Created by Andrew on 6/8/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "MenuVC.h"
#import "MapVC.h"
#import "MyGaragesVC.h"
#import "ProfileVC.h"
#import "SettingsVC.h"
#import "FacebookVC.h"
#import "TwitterVC.h"

@implementation MenuVC

- (void)dealloc
{
    [settingsBtn release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [settingsBtn release];
    settingsBtn = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)goToMap:(id)sender {
    MapVC *viewController = [[MapVC alloc] initWithNibName:@"MapVC" bundle:nil];
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:viewController animated:YES];
    [viewController release];
}

- (IBAction)goToMyLocations:(id)sender {
    MyGaragesVC *viewController = [[MyGaragesVC alloc] initWithNibName:@"MyGaragesVC" bundle:nil];
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:viewController animated:YES];
    [viewController release];
}

- (IBAction)goToMyProfile:(id)sender {
    ProfileVC *viewController = [[ProfileVC alloc] initWithNibName:@"ProfileVC" bundle:nil];
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:viewController animated:YES];
    [viewController release];
}

- (IBAction)activateFacebook:(id)sender {
    FacebookVC *viewController = [[FacebookVC alloc] initWithNibName:@"FacebookVC" bundle:nil];
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:viewController animated:YES];
    [viewController release];
}

- (IBAction)activateTwitter:(id)sender {
    TwitterVC *viewController = [[TwitterVC alloc] initWithNibName:@"TwitterVC" bundle:nil];
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:viewController animated:YES];
    [viewController release];
}

- (IBAction)goToSettings:(id)sender {
    SettingsVC *viewController = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:nil];
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:viewController animated:YES];
    [viewController release];
}
@end
