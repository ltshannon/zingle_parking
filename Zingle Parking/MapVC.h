//
//  FirstViewController.h
//  Zingle Parking
//
//  Created by Andrew on 5/11/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Mapkit/Mapkit.h>
#import "LimitedMapView.h"
#import "BSForwardGeocoder.h"
#import <CoreLocation/CoreLocation.h>

@interface MapVC : UIViewController <MKMapViewDelegate, UISearchBarDelegate, BSForwardGeocoderDelegate, UIAlertViewDelegate, CLLocationManagerDelegate> {

    IBOutlet UISearchBar *searchB;
    IBOutlet LimitedMapView *map;
    
    @private
    BSForwardGeocoder *forwardGeocoderSearch;
    BOOL hasCenteredInitial;
    CLLocationManager *cLManager;
   // NSArray * facilities;
}
@property(nonatomic, retain) LimitedMapView *map;

- (IBAction)back:(id)sender;
- (IBAction)centerOnUsersLocation:(id)sender;
@end
