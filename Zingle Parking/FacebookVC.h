//
//  FBLoginVC.h
//  Zingle Parking
//
//  Created by Andrew on 6/23/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"

@interface FacebookVC : UIViewController <FBRequestDelegate,FBDialogDelegate,FBSessionDelegate, 
UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    IBOutlet UIView *panelVw;
    IBOutlet FBLoginButton *fbButton;
    Facebook *facebook;
    
    IBOutlet UITextField *statusTf;
    IBOutlet UIButton *postBtn;
    IBOutlet UIButton *pictureBtn;
    IBOutlet UITextView *pictureText;
    
    @private
    
    NSArray *_permissions;
    UIActionSheet *selectSrc;
    UIImagePickerController *imgContr;
}
@property(nonatomic, retain) Facebook * facebook;

- (IBAction)back:(id)sender;
- (IBAction)fbButtonClicked:(id)sender;

-(void)login;
-(void)logout;

- (IBAction)postStatus:(id)sender;
- (IBAction)takePicture:(id)sender;
@end
