//
//  Constants.h
//  Zingle Parking
//
//  Created by Andrew on 5/23/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//


/*
 Constants used in saving the users last map region
 */
#define kLastMapRegion      @"last.map.region"
#define kLastMapSpanLat     @"last.map.span.lat"
#define kLastMapSpanLng     @"last.map.span.lng"
#define kLastMapCenterLat   @"last.map.center.lat"
#define kLastMapCenterLng   @"last.map.center.lng"

/*
 Constant used in remembering the active tab from last session
 */
#define kLastTabOpen        @"last.tab.open"

/*
 Constant used to determine the map type
 */
#define kMapTypePref        @"map.type.pref"

/*
 Constant used for users favorite garages
 */
#define kFavoriteGarages    @"favorite.garages"

/*
 Constant used to store users profile to disk
 */
#define kUserZingleProfile  @"user.zingle.profile"

/*
 Constant used to determine whether app launch requires a pin or not
 */
#define kPinRequired        @"pin.required"
#define kPin                @"user.pin"


/*
 Facebook
 */
#define kFBAppID @"219692151397028"

/*
 Twitter
 */
#define kOAuthConsumerKey @"A26gdwEO1brETE5tJcMBQ"
#define kOAuthConsumerSecret @"aLvnQJhrocMV2w9XYBM5TX7f8SeVwtwy35HNG8dyIE"	

/*
 API Constants
 */
#define kBaseAPIURL         @"http://www.zingleapps.com/sugar/access/feeds/parking.json"
#define kLocations          @"Locations"
#define kFacInSeat          @"In-Seat"
#define kFacLat             @"Lat"
#define kFacLng             @"Lng"
#define kFacPhone           @"Phone"
#define kFacZingle          @"Zingle"
#define kFacName            @"Name"
#define kFacNameSecondary   @"NameSecondary"
#define kFacWebsite         @"Website"
#define kFacMenu            @"Menu"
#define kFacAddress         @"Address"
#define kFacAddressStreet   @"Street"
#define kFacAddressCity     @"City"
#define kFacAddressState    @"State"
#define kFacAddressZip      @"ZipCode"
