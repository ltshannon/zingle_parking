//
//  DetailedGarageVC.h
//  Zingle Parking
//
//  Created by Andrew on 5/12/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "DTAttributedTextContentView.h"

@interface DetailedGarageVC : UIViewController <MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, UIWebViewDelegate>
{
    NSDictionary *garage;
    
    IBOutlet UIView *panelVw;
    IBOutlet DTAttributedTextContentView *detailsAttrVw;
    
    IBOutlet UIWebView *webView;
    
    IBOutlet UIButton *favoritesBtn;
    
    @private
    
    NSDate *_storedGetTime;
}
@property(nonatomic, retain) NSDictionary* garage;

-(int)isGarageInFavorites;

-(void)composeSMSMessageWithGetTime:(NSDate*)getTime andValet:(NSString*)valetStr;

- (IBAction)back:(id)sender;

-(IBAction)getNow;
-(IBAction)getAt;
- (IBAction)addRemoveToLocations:(id)sender;
@end
