//
//  FBLoginVC.m
//  Zingle Parking
//
//  Created by Andrew on 6/23/11.
//  Copyright 2011 Chimp Studios. All rights reserved.
//

#import "FacebookVC.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "Constants.h"

#define kCam @"Camera"
#define kAlb @"Saved Photos Album"
#define kLib @"Photo Library"

@implementation FacebookVC
@synthesize facebook;

- (void)dealloc
{
    [panelVw release];
    [fbButton release];
    [_permissions release];
    [statusTf release];
    [postBtn release];
    [pictureBtn release];
    [pictureText release];
    [facebook release];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    delegate.fbController = nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[panelVw layer] setCornerRadius:20];
    [[panelVw layer] setBorderColor:[[UIColor blackColor] CGColor]];
    [[panelVw layer] setBorderWidth:1];
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    delegate.fbController = self;
    
    facebook = [[[Facebook alloc] initWithAppId:kFBAppID] retain];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] 
        && [defaults objectForKey:@"FBExpirationDateKey"]) {
        facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
    }
    
    if (![facebook isSessionValid]) {
        //[facebook authorize:nil delegate:self];
        fbButton.isLoggedIn = NO;
        statusTf.enabled = NO;
        statusTf.alpha = 0.5;
        postBtn.enabled = NO;
        postBtn.alpha = 0.5;
        pictureBtn.enabled = NO;
        pictureBtn.alpha = 0.5;
        pictureText.alpha = 0.5;
        [fbButton updateImage];
    } else {
        fbButton.isLoggedIn = YES;
        statusTf.enabled = YES;
        statusTf.alpha = 1.0;
        postBtn.enabled = YES;
        postBtn.alpha = 1.0;
        pictureBtn.enabled = YES;
        pictureBtn.alpha = 1.0;
        pictureText.alpha = 1.0;
        
        [fbButton updateImage];
    }
}

- (void)viewDidUnload
{
    [panelVw release];
    panelVw = nil;
    [fbButton release];
    fbButton = nil;
    [statusTf release];
    statusTf = nil;
    [postBtn release];
    postBtn = nil;
    [pictureBtn release];
    pictureBtn = nil;
    [pictureText release];
    pictureText = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)back:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark FBConnect
- (IBAction)fbButtonClicked:(id)sender {
    if (fbButton.isLoggedIn) {
        [self logout];
    } else {
        [self login];
    }
}

/**
 * Show the authorization dialog.
 */
- (void)login {
    _permissions =  [[NSArray arrayWithObjects:
                      @"publish_stream", @"read_stream", @"offline_access", nil] retain];
    [facebook authorize:_permissions delegate:self];
}

/**
 * Invalidate the access token and clear the cookie.
 */
- (void)logout {
    [facebook logout:self];
}

- (IBAction)postStatus:(id)sender {
    [statusTf resignFirstResponder];
    
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    postBtn.alpha = 0.5;
    postBtn.enabled = NO;
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:1];
    [params setObject:statusTf.text forKey:@"message"];
   // [facebook dialog:@"feed" andParams:params andDelegate:self];
    [facebook requestWithGraphPath:@"/me/feed"
                         andParams:params
                     andHttpMethod:@"POST"
                       andDelegate:self];
    
    [params release];
}

- (IBAction)takePicture:(id)sender {
    
    BOOL phtlib, cam, svdalb;
	phtlib = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
	cam = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
	svdalb = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
	
	if (phtlib && cam && svdalb) {
		selectSrc = [[UIActionSheet alloc] initWithTitle:@"Photo Source"
												delegate:self
									   cancelButtonTitle:@"Cancel"
								  destructiveButtonTitle:kCam otherButtonTitles:kLib, kAlb, nil];
        
	}
	else if (phtlib && cam) {
		selectSrc = [[UIActionSheet alloc] initWithTitle:@"Photo Source"
												delegate:self
									   cancelButtonTitle:@"Cancel"
								  destructiveButtonTitle:kCam otherButtonTitles:kLib, nil];
	}
	else if (phtlib && svdalb) {
		selectSrc = [[UIActionSheet alloc] initWithTitle:@"Photo Source"
												delegate:self
									   cancelButtonTitle:@"Cancel"
								  destructiveButtonTitle:kAlb otherButtonTitles:kLib, nil];
	}
	else if (cam && svdalb) {
		selectSrc = [[UIActionSheet alloc] initWithTitle:@"Photo Source"
												delegate:self
									   cancelButtonTitle:@"Cancel"
								  destructiveButtonTitle:kCam otherButtonTitles:kAlb, nil];
	}
	else if (cam) {
		selectSrc = [[UIActionSheet alloc] initWithTitle:@"Photo Source"
												delegate:self
									   cancelButtonTitle:@"Cancel"
								  destructiveButtonTitle:kCam otherButtonTitles: nil];
	}
	else if (svdalb) {
		selectSrc = [[UIActionSheet alloc] initWithTitle:@"Photo Source"
												delegate:self
									   cancelButtonTitle:@"Cancel"
								  destructiveButtonTitle:kAlb otherButtonTitles: nil];
	}
	else if (phtlib) {
		selectSrc = [[UIActionSheet alloc] initWithTitle:@"Photo Source"
												delegate:self
									   cancelButtonTitle:@"Cancel"
								  destructiveButtonTitle:kLib otherButtonTitles: nil];
	} else {
		UIAlertView *noSrc = [[UIAlertView alloc] initWithTitle:@"No Photo Source Available"
														message:@"It appears that you do not have any photos stored on your device and you either do not have a camera or it is in use."
													   delegate:self
											  cancelButtonTitle:@"Okay"
											  otherButtonTitles:nil];
		[noSrc show];
		[noSrc release];
	}
    
	
	if (selectSrc) {
		[selectSrc showInView:self.view];
	}
}


#pragma mark UIActionSheetDelegate Method
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSString *btnTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
	if ([btnTitle isEqualToString:kCam]) {
		imgContr = [[UIImagePickerController alloc] init];
		[imgContr setSourceType:UIImagePickerControllerSourceTypeCamera];
	} else if ([btnTitle isEqualToString:kLib]) {
		imgContr = [[UIImagePickerController alloc] init];
		[imgContr setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
	} else if ([btnTitle isEqualToString:kAlb]) {
		imgContr = [[UIImagePickerController alloc] init];
		[imgContr setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
	} else {
		[selectSrc release];
		return;
	}
	
	
	if (imgContr) {
		imgContr.view.frame = self.view.frame;
		imgContr.allowsEditing = YES;
		imgContr.delegate = self;
		
		//ParkingAppDelegate *delegate = (ParkingAppDelegate*)[[UIApplication sharedApplication] delegate];
		
		[self presentModalViewController:imgContr animated:YES];
	}
	[selectSrc release];    
}

#pragma mark UIImagePickerControllerDelegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	
	UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
	
	if (!image) {
		image = [info objectForKey:UIImagePickerControllerOriginalImage];
	}
	
	[picker dismissModalViewControllerAnimated:YES];
	
	
	if (image) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        NSData *imgData = UIImageJPEGRepresentation(image, 1.0);
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       imgData, @"picture",
                                       nil];
        
        
        pictureBtn.alpha = 0.5;
        pictureBtn.enabled = NO;
        
        [facebook requestWithGraphPath:@"me/photos"
                             andParams:params
                         andHttpMethod:@"POST"
                           andDelegate:self];
        
        
	}
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[picker dismissModalViewControllerAnimated:YES];
}


/**
 * Called when the user has logged in successfully.
 */
- (void)fbDidLogin {
    fbButton.isLoggedIn = YES;
    [fbButton updateImage];
    
    // save the fb login credentials to disk
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    
    
    statusTf.enabled = YES;
    statusTf.alpha = 1.0;
    postBtn.enabled = YES;
    postBtn.alpha = 1.0;
    pictureBtn.enabled = YES;
    pictureBtn.alpha = 1.0;
    pictureText.alpha = 1.0;
}

/**
 * Called when the user canceled the authorization dialog.
 */
-(void)fbDidNotLogin:(BOOL)cancelled {
}

/**
 * Called when the request logout has succeeded.
 */
- (void)fbDidLogout {
    fbButton.isLoggedIn = NO;
    [fbButton updateImage];
    
    statusTf.enabled = NO;
    statusTf.alpha = 0.5;
    postBtn.enabled = NO;
    postBtn.alpha = 0.5;
    pictureBtn.enabled = NO;
    pictureBtn.alpha = 0.5;
    pictureText.alpha = 0.5;
    
}

////////////////////////////////////////////////////////////////////////////////
// FBRequestDelegate

/**
 * Called when the Facebook API request has returned a response. This callback
 * gives you access to the raw response. It's called before
 * (void)request:(FBRequest *)request didLoad:(id)result,
 * which is passed the parsed response object.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
   // NSLog(@"received response");
}

/**
 * Called when a request returns and its response has been parsed into
 * an object. The resulting object may be a dictionary, an array, a string,
 * or a number, depending on the format of the API response. If you need access
 * to the raw response, use:
 *
 * (void)request:(FBRequest *)request
 *      didReceiveResponse:(NSURLResponse *)response
 */
- (void)request:(FBRequest *)request didLoad:(id)result {
    if ([result isKindOfClass:[NSArray class]]) {
        result = [result objectAtIndex:0];
    }
    //NSLog(@"%@", result);
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    postBtn.alpha = 1.0;
    postBtn.enabled = YES;
    
    pictureBtn.alpha = 1.0;
    pictureBtn.enabled = YES;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success:"
                                                    message:@"Your post has been uploaded to Facebook"
                                                   delegate:nil
                                          cancelButtonTitle:@"Okay"
                                          otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
};

/**
 * Called when an error prevents the Facebook API request from completing
 * successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    postBtn.alpha = 1.0;
    postBtn.enabled = YES;
    
    pictureBtn.alpha = 1.0;
    pictureBtn.enabled = YES;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error:"
                                                    message:[error description]
                                                   delegate:nil
                                          cancelButtonTitle:@"Okay"
                                          otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
};


////////////////////////////////////////////////////////////////////////////////
// FBDialogDelegate

/**
 * Called when a UIServer Dialog successfully return.
 */
- (void)dialogDidComplete:(FBDialog *)dialog {
    //NSLog(@"publish successfully");
    statusTf.text = @"";
    statusTf.placeholder = @"Successfully Posted!";
}


@end
